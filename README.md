# Multi Protocol Bouncer

## Summary
Scripts that bounce chats from different protocols to self hosted servers, such as from social media to specialized IRC networks.

## Status
The founder of this project, Andrew <andrew@andrewyu.org>, is making this project initially with IRC and WeChat. 

His aim is to make a IRC server (in Python at first, might convert to Lua if it's too slow) that handles and gives you WeChat messages, just like how [ZNC](https://znc.in) bounces IRC messages

Glad if you can give me help :)

